CREATE DATABASE IF NOT EXISTS TimeToLearn;

USE TimeToLearn;

DROP TABLE IF EXISTS allusers;
DROP TABLE IF EXISTS lesson;
DROP TABLE IF EXISTS stu_lesson;
DROP TABLE IF EXISTS school;
DROP TABLE IF EXISTS school_class;
DROP TABLE IF EXISTS teacher_class;


CREATE TABLE school
(
schoolId INT(5) PRIMARY KEY AUTO_INCREMENT,
school_name VARCHAR(55) NOT NULL,
town VARCHAR(55) NOT NULL UNIQUE,
county VARCHAR(55) NOT NULL UNIQUE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE school_class
(
school_classid INT (5) PRIMARY KEY AUTO_INCREMENT,
class_name VARCHAR(55) NOT NULL,
schoolId INT(5) NOT NULL UNIQUE

CONSTRAINT fk_school_class_schoolId FOREIGN KEY(schoolId) REFERENCES school(schoolId)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE allusers
(
userId INT (5) PRIMARY KEY AUTO_INCREMENT,
uname VARCHAR (25) NOT NULL UNIQUE,
pword VARCHAR (25) NOT NULL,
fname VARCHAR (20) NOT NULL,
lname VARCHAR(20) NOT NULL,
email VARCHAR (35) NOT NULL,
classId INT(5) NOT NULL UNIQUE,
isTeacher BOOLEAN NOT NULL

CONSTRAINT fk_allusers_classId FOREIGN KEY(classId) REFERENCES school_class (school_classid)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE lesson
(
lessonId    int(5) NOT NULL PRIMARY KEY AUTO_INCREMENT,
lesson_name  varchar(25) NOT NULL

) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE stu_lesson
(
stu_lessonId int(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
lessonId INT(5) NOT NULL UNIQUE,
stuId int(5) NOT NULL UNIQUE, 
score int(5) NOT NULL,
lesson_date  date NOT NULL

CONSTRAINT fk_lessonId FOREIGN KEY(lessonId) REFERENCES lesson (lessonId),
CONSTRAINT fk_stu_lesson FOREIGN KEY(stuId) REFERENCES allusers (userId)

) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE teacher_class
(
id INT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
teacherid INT(5) NOT NULL UNIQUE,
classid INT(5) NOT NULL UNIQUE

CONSTRAINT fk_teacherid FOREIGN KEY(teacherid) REFERENCES allusers (userId),
CONSTRAINT fk_teacher_classid FOREIGN KEY(classid) REFERENCES school_class (classid)

) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (1,"lassie","ringo","Mary","Martin","mary@bal.ie",1,true);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (2,"rocky","sean","Ivor","Martin","ivor@gmail.com",5,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (3,"tomcat","tc","Matthew","Lynn","mat@gmail.com",5,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (4,"house","door","Mary","Reid","mary@live.ie",2,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (5,"car","wheel","Joseph","Roddy","sean@dkit.ie",2,true);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (6,"pepper","salt","Roisin","James","rj@gmail.com",2,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (7,"vox","mixer","David","Sullivan","sully@covewell.ie",1,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (8,"tango","trot","Joeanne","Dowdall","joeanne@live.ie",1,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (9,"liam","barns","Frank","Sugrue","frankS@eircom.net",1,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (10,"bus","driver","PJ","Murphy","liam@dkit.ie",3,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (11,"dogs","ringo","Sarah","McGrane","sarah@gmail.ie",3,true);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (12,"keychain","sean","Sean","Francis","fran@gmail.com",5,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (13,"leapfrog","leap","Rory","Lynch","rory@gmail.com",5,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (14,"madhouse","doom","Mary","Rankin","reids@live.ie",2,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (15,"flashcar","wheelly","Bobby","Rodens","bob@dkit.ie",4,true);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (16,"pepper","salt&","Roisin","Jackson","roisin@gmail.com",4,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (17,"roxy","mixed","Danny","Boyle","dannie@carefree.ie",1,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (18,"alpha","trots","Sasha","O'Keefe","sasha@live.ie",4,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (19,"abba","barns","Frankie","McGinn","frankG@eircom.net",1,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (20,"busstop","driverTom","Banjo","Montaf","banjo@dkit.ie",3,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (21,"buckaroo","rings","Emma","Maguire","emmy@baltra.ie",5,true);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (22,"jobless","seanM","Ivan","Masterson","ivan@gmail.com",5,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (23,"topcat","tpc","Ronny","Long","ronn@gmail.com",5,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (24,"partytime","doris","Marion","O'Sullivan","mario@live.ie",2,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (25,"carts","wheels","Jose","Rice","jose@dkit.ie",6,true);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (26,"pepperpot","salty","Rose","Jameson","rose@gmail.com",2,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (27,"vickyt","robots","David","Litchfield","dave@covewell.ie",1,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (28,"tangoB","trolls","Jane","Dowdall","joeanne@live.ie",1,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (29,"liamS","barnyard","John","Sugrue","frankS@eircom.net",3,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (30,"busman","driven","Paul","Murphy","liam@dkit.ie",3,false);

INSERT INTO lesson 
VALUES(1, "Minutes");
INSERT INTO lesson 
VALUES(2, "Hours");
INSERT INTO lesson 
VALUES(3, "Past");
INSERT INTO lesson 
VALUES(4, "To");


commit;
