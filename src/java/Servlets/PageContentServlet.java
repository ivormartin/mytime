/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Servlets;
import AjaxCommands.*;
import java.io.IOException;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;

/**
 *
 *@servlet implementation class RelatedContentServlet
 */
@WebServlet(name = "PageContentServlet", urlPatterns = {"/PageContentServlet"})
public class PageContentServlet extends HttpServlet {
        
       private static final long serialVersionUID = 1L;
       
    public PageContentServlet() 
    {
        super();
    }
    
        
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }    
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    
      String result = "";
      //ServletContext context = null;
      if ( request.getParameter("action") != null)
        {
      
            //content = request.getParameter("content");
            AjaxCommandFactory factory = AjaxCommandFactory.getInstance();
           
            AjaxCommand command = factory.createCommand(request.getParameter("action"));
       
           response.setContentType("text/plain");
           response.setCharacterEncoding("UTF-8");
           result = command.execute(request, response);
        }
        
        //Get the request dispatcher object and forward the request to the appropriate JSP page...
        //RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(result);
        //dispatcher.forward(request,response);
        // B) Redirecting approach:
        //response.sendRedirect(result);
}
      
    }