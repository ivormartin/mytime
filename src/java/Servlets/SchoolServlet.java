/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Servlets;
import Commands.*;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;
/**
 *
 * @servlet implementation class SchoolServlet
 */
@WebServlet(urlPatterns = {"/SchoolServlet"})
public class SchoolServlet extends HttpServlet {

    	private static final long serialVersionUID = 1L;

    public SchoolServlet() 
    {
        super();
    }
    
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String forwardToJsp = "";
        
        //Check the 'action' parameter to see what the user wants...
 if ( request.getParameter("action") != null)
        {
            
            CommandFactory factory = CommandFactory.getInstance();
            Command command = factory.createCommand(request.getParameter("action"));
            
            forwardToJsp = command.execute(request, response);
            
        }
        
        //Get the request dispatcher object and forward the request to the appropriate JSP page...
        /*RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/"+forwardToJsp);
        dispatcher.forward(request,response);*/
        // B) Redirecting approach:
        response.sendRedirect(forwardToJsp);
}
     
}