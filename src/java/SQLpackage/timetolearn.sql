CREATE DATABASE IF NOT EXISTS timetolearn;

USE timetolearn;

DROP TABLE IF EXISTS stu_lesson;
DROP TABLE IF EXISTS lesson;
DROP TABLE IF EXISTS teacher_school;
DROP TABLE IF EXISTS allusers;
DROP TABLE IF EXISTS school_class;
DROP TABLE IF EXISTS school;

CREATE TABLE school
(
schoolId INT(5) PRIMARY KEY AUTO_INCREMENT,
school_name VARCHAR(55) NOT NULL,
town VARCHAR(55) NOT NULL,
county VARCHAR(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE school_class
(
school_classid INT (5) PRIMARY KEY AUTO_INCREMENT,
class_name VARCHAR(55) NOT NULL,
schoolId INT(5) NOT NULL,
CONSTRAINT fk_schoolclass_schoolId FOREIGN KEY(schoolId) REFERENCES school(schoolId)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE allusers
(
userId INT (5) PRIMARY KEY AUTO_INCREMENT,
uname VARCHAR (25) NOT NULL UNIQUE,
pword VARCHAR (25) NOT NULL,
fname VARCHAR (20) NOT NULL,
lname VARCHAR(20) NOT NULL,
email VARCHAR (35) NOT NULL,
classId INT(5) NOT NULL,
isTeacher BOOLEAN NOT NULL,

CONSTRAINT fk_allusers_classId FOREIGN KEY(classId) REFERENCES school_class (school_classid)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE lesson
(
lessonId    INT(5) NOT NULL PRIMARY KEY AUTO_INCREMENT,
lesson_name  VARCHAR(25) NOT NULL

) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE stu_lesson
(
stu_lessonId INT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
lessonId INT(5) NOT NULL,
stuId INT(5) NOT NULL, 
score INT(5) NOT NULL,
lesson_date  date NOT NULL,

CONSTRAINT fk_lessonId FOREIGN KEY(lessonId) REFERENCES lesson (lessonId),
CONSTRAINT fk_stu_lesson FOREIGN KEY(stuId) REFERENCES allusers (userId)

) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE teacher_school
(
id INT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
teacherid INT(5) NOT NULL UNIQUE,
schoolid INT(5) NOT NULL UNIQUE,

CONSTRAINT fk_teacherid FOREIGN KEY(teacherid) REFERENCES allusers (userId),
CONSTRAINT fk_teacher_schoolid FOREIGN KEY(schoolid) REFERENCES school (schoolid)

) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TRIGGER IF EXISTS deleteLessonTrigger ; 
DELIMITER //
CREATE TRIGGER deleteLessonTrigger 
BEFORE DELETE ON allusers
FOR EACH ROW
BEGIN 
    DECLARE existingStuLesson INT;
    DECLARE dummyData VARCHAR(32);
        SELECT COUNT(*) 
        FROM stu_lesson
        WHERE stu_lesson.stuId = OLD.userId INTO existingStuLesson;
        IF existingStuLesson>0 THEN
            DELETE FROM stu_lesson WHERE stu_lesson.stuId = OLD.userId;
        ELSE
            SET dummyData = "Something to do";
        END IF;
END //
DELIMITER ;


DROP PROCEDURE IF EXISTS logHighestStuScore;
DELIMITER //
CREATE PROCEDURE logHighestStuScore
(
    IN lessonExists BOOLEAN,
    IN studentid INTEGER,
    IN highScore INTEGER
)
BEGIN
     DECLARE oldScore INTEGER;
     DECLARE someData VARCHAR(32);
     IF lessonExists THEN
        SELECT score FROM stu_lesson 
        WHERE stuId = studentid INTO oldScore;
        IF newScore > oldScore THEN
            UPDATE stu_lesson
            SET score = newScore
            WHERE stuId = studentid;
        ELSE
            SET someData = "Something to do";
        END IF;
      ELSE
            SET someData = "Something else to do";
      END IF;
END //
DELIMITER ;
     

DROP PROCEDURE IF EXISTS loadusers;
DELIMITER //
CREATE PROCEDURE loadusers
(
IN theUname VARCHAR (25),
IN thePword VARCHAR (25),
IN theFname VARCHAR (20),
IN theLname VARCHAR(20),
IN theEmail VARCHAR (35),
IN theClassId INT(5),
IN teaches BOOLEAN
)
BEGIN
    INSERT INTO allusers
    (uname,pword,fname,lname,email,classId,isTeacher)
    VALUES
    (theUname,thePword,theFname,theLname,theEmail,theClassId,teaches);
END //
DELIMITER ;

