
DELIMITER //
CREATE PROCEDURE default_student
(
    OUT lastid INTEGER
)
BEGIN
            SELECT LAST_INSERT_ID FROM allusers WHERE isTeacher = 0 INTO lastid;
      
END //
DELIMITER ;