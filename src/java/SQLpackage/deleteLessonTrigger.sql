DROP TRIGGER IF EXISTS deleteLessonTrigger ; 
DELIMITER //
CREATE TRIGGER deleteLessonTrigger 
AFTER DELETE ON allUsers
FOR EACH ROW
BEGIN 
        /*SELECT COUNT(*) AS count FROM stu_lesson WHERE stuId=OLD.userId
        IF count>0 THEN*/
            DELETE * FROM stu_lesson WHERE stuId=OLD.userId;
	/*END IF; */
END //
DELIMITER ;
