INSERT INTO school(schoolId,school_name,town,county)
VALUES(1,"St.Marys","Dundalk","Louth");
INSERT INTO school(schoolId,school_name,town,county)
VALUES(2,"St.Marys","Ardee","Louth");
INSERT INTO school(schoolId,school_name,town,county)
VALUES(3,"St.Marys","Navan","Meath");
INSERT INTO school(schoolId,school_name,town,county)
VALUES(4,"St.Marys","Kells","Cavan");
INSERT INTO school(schoolId,school_name,town,county)
VALUES(5,"St.Marys","Donabate","Dublin");
INSERT INTO school(schoolId,school_name,town,county)
VALUES(6,"The Bohar","Grange","Louth");
INSERT INTO school(schoolId,school_name,town,county)
VALUES(7,"St.Annes","Navan","Meath");
INSERT INTO school(schoolId,school_name,town,county)
VALUES(8,"Milgrange","Greenore","Louth");
INSERT INTO school(schoolId,school_name,town,county)
VALUES(9,"St. Johns","Ballybay","Cavan");
INSERT INTO school(schoolId,school_name,town,county)
VALUES(10,"Friary","Dundalk","Louth");

INSERT INTO school_class (school_classid,class_name,schoolId)
VALUES(1,"Primary1",1);
INSERT INTO school_class (school_classid,class_name,schoolId)
VALUES(2,"Primary2",1);
INSERT INTO school_class (school_classid,class_name,schoolId)
VALUES(3,"Primary3",1);
INSERT INTO school_class (school_classid,class_name,schoolId)
VALUES(4,"Primary4",1);
INSERT INTO school_class (school_classid,class_name,schoolId)
VALUES(5,"primary1",2);
INSERT INTO school_class (school_classid,class_name,schoolId)
VALUES(6,"primary2",2);
INSERT INTO school_class (school_classid,class_name,schoolId)
VALUES(7,"primary3",2);
INSERT INTO school_class (school_classid,class_name,schoolId)
VALUES(8,"primary4",2);
INSERT INTO school_class (school_classid,class_name,schoolId)
VALUES(9,"primary1",3);
INSERT INTO school_class (school_classid,class_name,schoolId)
VALUES(10,"primary2",3);
INSERT INTO school_class (school_classid,class_name,schoolId)
VALUES(11,"primary3",3);
INSERT INTO school_class (school_classid,class_name,schoolId)
VALUES(12,"primary4",3);
INSERT INTO school_class (school_classid,class_name,schoolId)
VALUES(13,"primary1",4);
INSERT INTO school_class (school_classid,class_name,schoolId)
VALUES(14,"primary2",4);
INSERT INTO school_class (school_classid,class_name,schoolId)
VALUES(15,"primary3",4);
INSERT INTO school_class (school_classid,class_name,schoolId)
VALUES(16,"primary4",4);

INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (1,"lassie","ringo","Mary","Martin","mary@bal.ie",1,true);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (2,"rocky","sean","Ivor","Martin","ivor@gmail.com",5,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (3,"tomcat","tc","Matthew","Lynn","mat@gmail.com",5,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (4,"house","door","Mary","Reid","mary@live.ie",2,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (5,"car","wheel","Joseph","Roddy","sean@dkit.ie",2,true);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (6,"pepper","salt","Roisin","James","rj@gmail.com",2,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (7,"vox","mixer","David","Sullivan","sully@covewell.ie",1,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (8,"tango","trot","Joeanne","Dowdall","joeanne@live.ie",1,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (9,"liam","barns","Frank","Sugrue","frankS@eircom.net",1,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (10,"bus","driver","PJ","Murphy","liam@dkit.ie",3,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (11,"dogs","ringo","Sarah","McGrane","sarah@gmail.ie",3,true);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (12,"keychain","sean","Sean","Francis","fran@gmail.com",5,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (13,"leapfrog","leap","Rory","Lynch","rory@gmail.com",5,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (14,"madhouse","doom","Mary","Rankin","reids@live.ie",2,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (15,"flashcar","wheelly","Bobby","Rodens","bob@dkit.ie",4,true);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (16,"max","salt&","Roisin","Jackson","roisin@gmail.com",4,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (17,"roxy","mixed","Danny","Boyle","dannie@carefree.ie",1,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (18,"alpha","trots","Sasha","OKeefe","sasha@live.ie",4,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (19,"abba","barns","Frankie","McGinn","frankG@eircom.net",1,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (20,"busstop","driverTom","Banjo","Montaf","banjo@dkit.ie",3,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (21,"buckaroo","rings","Emma","Maguire","emmy@baltra.ie",5,true);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (22,"jobless","seanM","Ivan","Masterson","ivan@gmail.com",5,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (23,"topcat","tpc","Ronny","Long","ronn@gmail.com",5,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (24,"partytime","doris","Marion","Sullivan","mario@live.ie",2,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (25,"carts","wheels","Jose","Rice","jose@dkit.ie",6,true);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (26,"pepperpot","salty","Rose","Jameson","rose@gmail.com",2,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (27,"vickyt","robots","David","Litchfield","dave@covewell.ie",1,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (28,"tangoB","trolls","Jane","Dowdall","joeanne@live.ie",1,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (29,"liamS","barnyard","John","Sugrue","frankS@eircom.net",3,false);
INSERT INTO allusers (userId,uname,pword,fname,lname,email,classid,isTeacher)
VALUES (30,"busman","driven","Paul","Murphy","liam@dkit.ie",3,false);

INSERT INTO lesson 
VALUES(1, "Minutes");
INSERT INTO lesson 
VALUES(2, "Hours");
INSERT INTO lesson 
VALUES(3, "Past");
INSERT INTO lesson 
VALUES(4, "To");

INSERT INTO stu_lesson
VALUES(1,1,2,4,'2014-03-12');
INSERT INTO stu_lesson
VALUES(2,1,3,5,'2014-02-28');
INSERT INTO stu_lesson
VALUES(3,1,4,5,'2014-01-21');
INSERT INTO stu_lesson
VALUES(4,1,6,7,'2014-01-14');
INSERT INTO stu_lesson
VALUES(5,1,7,4,'2014-03-25');
INSERT INTO stu_lesson
VALUES(6,1,8,8,'2014-04-09');
INSERT INTO stu_lesson
VALUES(7,1,9,6,'2014-02-27');
INSERT INTO stu_lesson
VALUES(8,1,10,8,'2014-01-19');
INSERT INTO stu_lesson
VALUES(9,1,12,9,'2014-02-11');
INSERT INTO stu_lesson
VALUES(10,1,13,4,'2014-03-12');
INSERT INTO stu_lesson
VALUES(11,1,14,5,'2014-02-28');
INSERT INTO stu_lesson
VALUES(12,1,16,5,'2014-01-21');
INSERT INTO stu_lesson
VALUES(13,1,17,7,'2014-01-14');
INSERT INTO stu_lesson
VALUES(14,1,18,4,'2014-03-25');
INSERT INTO stu_lesson
VALUES(15,1,19,8,'2014-04-09');
INSERT INTO stu_lesson
VALUES(16,1,20,6,'2014-02-27');
INSERT INTO stu_lesson
VALUES(17,1,22,8,'2014-01-19');
INSERT INTO stu_lesson
VALUES(21,1,23,5,'2014-01-21');
