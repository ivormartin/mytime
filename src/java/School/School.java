/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package School;
/*
 * @author user
 */
public class School {
   int schoolid;
   String schoolName;
   String town;
   String county;
   
   public School(int schId,String schName,String town,String county){
       this.schoolid = schId;
       this.schoolName = schName;
       this.town = town;
       this.county = county;
   }

    public int getSchoolid() {
        return schoolid;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getTown() {
        return town;
    }

    public String getCounty() {
        return county;
    }

    public void setSchoolid(int schoolid) {
        this.schoolid = schoolid;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.schoolid;
        hash = 97 * hash + (this.schoolName != null ? this.schoolName.hashCode() : 0);
        hash = 97 * hash + (this.town != null ? this.town.hashCode() : 0);
        hash = 97 * hash + (this.county != null ? this.county.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final School other = (School) obj;
        if (this.schoolid != other.schoolid) {
            return false;
        }
        if ((this.schoolName == null) ? (other.schoolName != null) : !this.schoolName.equals(other.schoolName)) {
            return false;
        }
        if ((this.town == null) ? (other.town != null) : !this.town.equals(other.town)) {
            return false;
        }
        if ((this.county == null) ? (other.county != null) : !this.county.equals(other.county)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "School{" + "schoolid=" + schoolid + ", schoolName=" + schoolName + ", town=" + town + ", county=" + county + '}';
    }
}