/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package School;

/*
 * @author user
 */
public class Lesson {
    private int lessonId;
    private String lessonName;

    public Lesson(int id,String n) {
        this.lessonId = id;
        this.lessonName = n;
    }

    public int getLessonId() {
        return lessonId;
    }

    public String getLessonName() {
        return lessonName;
    }

    public void setLessonId(int lessonId) {
        this.lessonId = lessonId;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }

    @Override
    public String toString() {
        return "lesson{" + "lessonId=" + lessonId + ", lessonName=" + lessonName + '}';
    }
  
}
