/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package School;
import java.sql.Date;
import java.util.GregorianCalendar;

/*
 * @author user
 */
public class StuLesson {
    
    private int lessonId;
    private int stuId;
    private int score;
    private java.sql.Date lessonDate;

    
    public StuLesson(int lessonId,int stuId,int score,Date lessonDate) {
        this.lessonId = lessonId;
        this.stuId = stuId;
        this.score = score;
        this.lessonDate = lessonDate;
    }
   
    public int getLessonId(){
        return lessonId;
    }
    
    public int getStuId() {
        return stuId;
    }

    public int getScore() {
        return score;
    }

    public Date getLessonDate() {
        return lessonDate;
    }

    public void setLessonId(int lessonId){
        this.lessonId = lessonId;
    }

    public void setStuId(int stuId) {
        this.stuId = stuId;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setLessonDate(Date lessonDate) {

        this.lessonDate = lessonDate;
    }
    
}
