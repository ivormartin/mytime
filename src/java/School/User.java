/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package School;

/**
 *
 * @author user
 */
public class User {
    
    private int userId;
    private String uname;
    private String pword;
    private String fname;
    private String lname;
    private String email;
    private int classID;
    private Boolean isTeacher;

    public User (int userId,String uname, String pword, String fname, String lname, String email,int classID,Boolean isTeacher) {
        this.userId = userId;
        this.uname = uname;
        this.pword = pword;
        this.fname = fname;
        this.lname = lname;
        this.email = email;
        this.classID = classID;
        this.isTeacher = isTeacher;
    }

    public int getUserId(){
        return userId;
    }

    public String getUname() {
        return uname;
    }

    public String getPword() {
        return pword;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public String getEmail() {
        return email;
    }

    public int getClassID(){
        return classID;
    }
    
    public boolean getType(){
        return isTeacher;
    }
    

    
    public void setUname(String uname) {
        this.uname = uname;
    }

    public void setPword(String pword) {
        this.pword = pword;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public void setClassID(int cID){
        this.classID = cID;
    }
    
    public void setUserType(boolean type){
      this.isTeacher = type;  
    }
    
    @Override
    public String toString() {
        return "User {uname= " + uname + ", pword= " + pword + ", fname= " + fname + ", lname= " + lname + ", email= " + email +", classID= "+ classID + ", isTeacher= " + isTeacher+'}';
    }

   
    
    
}
