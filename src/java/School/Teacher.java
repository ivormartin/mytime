/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package School;

/**
 *
 * @author user
 */
public class Teacher {
  int id;
  int teacherID;
  int schID;

    public Teacher(int id,int teacherID, int schID) {
        this.id = id;
        this.teacherID = teacherID;
        this.schID = schID;
    }

    public int getTeacherID() {
        return teacherID;
    }

    public int getSchID() {
        return schID;
    }

    public void setTeacherID(int teacherID) {
        this.teacherID = teacherID;
    }

    public void setSchID(int schID) {
        this.schID = schID;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.teacherID;
        hash = 97 * hash + this.schID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Teacher other = (Teacher) obj;
        if (this.teacherID != other.teacherID) {
            return false;
        }
        if (this.schID != other.schID) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Teacher{" + "teacherID=" + teacherID + ", schID=" + schID + '}';
    }
  
  
}
