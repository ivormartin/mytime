/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package School;

/**
 *
 * @author user
 */
public class SchoolClass {
   
   int classid;
   int schoolid;
   String classname;
   
   
   public SchoolClass(int classid,int schoolid,String classname){
       this.classid = classid;
       this.schoolid = schoolid;
       this.classname = classname;
   }

    public int getClassid() {
        return classid;
    }

    public int getSchoolid() {
        return schoolid;
    }

    public String getClassname() {
        return classname;
    }

    public void setSchoolid(int schoolid) {
        this.schoolid = schoolid;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + this.classid;
        hash = 73 * hash + this.schoolid;
        hash = 73 * hash + (this.classname != null ? this.classname.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SchoolClass other = (SchoolClass) obj;
        if (this.classid != other.classid) {
            return false;
        }
        if (this.schoolid != other.schoolid) {
            return false;
        }
        if ((this.classname == null) ? (other.classname != null) : !this.classname.equals(other.classname)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SchoolClass{" + "classid=" + classid + ", schoolid=" + schoolid + ", classname=" + classname + '}';
    }
   
   
}
