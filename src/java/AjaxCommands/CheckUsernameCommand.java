/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package AjaxCommands;

import Exceptions.DaoException;
import School.User;
import SchoolDao.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class CheckUsernameCommand implements AjaxCommand{
    
    public String execute(HttpServletRequest request, HttpServletResponse response) 
    {
        System.out.println("inside checkusernamecommand");
        ServletContext context = null;
        String theUsername = request.getParameter("content");
        String isNameOk ="";
       
            AjaxDao userService = new AjaxDao();
         
            try{
           
            isNameOk =  userService.checkUsernames(theUsername);
            System.out.println(isNameOk);
            response.setContentType("text/plain");
            response.setCharacterEncoding("UTF-8");
     
            PrintWriter pw = response.getWriter();
            pw.print(isNameOk);
 
            pw.flush();
            //context.getRequestDispatcher("/Register.jsp").forward(request, response);
            }
            catch(DaoException e){
                e.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(CheckUsernameCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return isNameOk;
    } 
    
}
