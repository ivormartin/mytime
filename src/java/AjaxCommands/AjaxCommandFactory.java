/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package AjaxCommands;

import AjaxCommands.AjaxCommand;

/**
 *
 * @author user
 */
public class AjaxCommandFactory {
    
    private static AjaxCommandFactory factory = null;
    
    private AjaxCommandFactory() {
    }
	
    public static synchronized AjaxCommandFactory getInstance()
    {
        if(factory == null)
        {
            factory = new AjaxCommandFactory();
        }
        return factory;
    }
    
    public synchronized AjaxCommand createCommand(String commandStr) 
    {
        
    	AjaxCommand command = null;
    	
	//Instantiate the required Command object...
    	if (commandStr.equals("username")) {
    		command = new CheckUsernameCommand();
    	}
//    	if (commandStr.equals("register")) {
//    		command = new RegisterCommand();
//    	}
//        if(commandStr.equals("lesson")){
//                command = new LessonCommand();
//        }
//        if(commandStr.equals("list")){
//                command = new ListCommand();
//        }
//        if(commandStr.equals("view")){
//                command = new ViewCommand();
//        }
//         if(commandStr.equals("delete")){
//                command = new DeleteCommand();
//        }
//         if(commandStr.equals("edit")){
//                command = new EditCommand();
//        }
//         if(commandStr.equals("create")){
//                command = new CreateCommand();
//        }
//         if(commandStr.equals("logout")){
//                command = new LogoutCommand();
//        } 
    	//Return the instantiated Command object to the calling code...
    	return command;// may be null
    }    
    
}