/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MongodbPackage;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;
import java.util.Arrays;
import java.util.Set;
/*
 * @author user
 */
public class MongoDBJDBC{
   public static void main( String args[] ){
       DBCollection coll;
      try{   
		 // To connect to mongodb server
         MongoClient mongoClient = new MongoClient("localhost",27017);
         // Now connect to your databases
         DB db = mongoClient.getDB("mydb");
		 //System.out.println("Connect to database successfully");
          coll = db.getCollection("logStore");
         
        BasicDBObject doc = new BasicDBObject("name", "MongoDB").append("type", "database")
                                                         .append("count", 1)
                                                         .append("info", new BasicDBObject("x", 203).append("y", 102));
        //doc.append("date","logfile");
        coll.insert(doc);
        DBObject myDoc = coll.findOne();
        System.out.println(myDoc);
        DBCursor cursor = coll.find();
    while (cursor.hasNext()) {
        DBObject updateDocument = cursor.next();
        updateDocument.put("likes","200");
        //coll.update(myDoc, updateDocument);
    }
System.out.println("Document updated successfully");
cursor = coll.find();
int i=1;
while (cursor.hasNext()) {
System.out.println("Updated Document: "+i);
System.out.println(cursor.next());
i++;
}
        
      }catch(Exception e){
	     System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
   }
}