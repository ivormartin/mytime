/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MongodbPackage;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author user
 */
public class MongoDocs extends MongoConnection{
    
    public List<String> getLogs(){
        
        DBObject obj = null;
        DBCollection coll;
        BasicDBObject doc;
        DBCursor cursor;
        DB db = null;
        List<String> logList = null;
           try{
            logList = new ArrayList();
            db = getMongoConnection();
            coll = db.getCollection("logStore");
            cursor = coll.find();
            //obj = cursor.next();
        while(cursor.hasNext()){
            //String level = (String) cursor.next().get("loglevel");
            String message = (String) cursor.next().get( "message" );
            //logList.add(level+" "+message);
            logList.add(message);
        }
        cursor.close();
    // Do Something...

        }catch(UnknownHostException e){
	     System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
           return logList;
    }
    
    public void removeDoc(){
        DBObject obj = null;
        DBCollection coll;
        DB db = null;
        try{
            db = getMongoConnection();
            coll = db.getCollection("logStore");
            obj = coll.findOne();
            coll.remove(obj);
        }catch(UnknownHostException e){
	     System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }
}
