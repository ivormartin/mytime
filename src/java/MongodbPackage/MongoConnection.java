/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MongodbPackage;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import java.net.UnknownHostException;

/*
 * @author user
 */
public class MongoConnection {
    
    public DB getMongoConnection() throws UnknownHostException
    {
        DB db = null;
        try{   
		 // To connect to mongodb server
         MongoClient mongoClient = new MongoClient("localhost",27017);
         // Now connect to your databases
            db = mongoClient.getDB("mydb");
        }
        catch(UnknownHostException e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        return db;
    } 
}
