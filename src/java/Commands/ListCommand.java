/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Commands;
import School.Lesson;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Exceptions.*;
import School.*;
import SchoolDao.*;
import java.util.List;
/**
 *
 * @author user
 */
public class ListCommand implements Command{
    
    public String execute(HttpServletRequest request, HttpServletResponse response) 
    {
        //Check to see if the session id coming from the client matches the id stored at login...
        HttpSession session = request.getSession();
        String forwardToJsp;

        //User not logged in...
        if ( session.getId() != session.getAttribute("loggedSessionId") )
        {
            forwardToJsp = "logIn.jsp";
        }
        else
        {	
            UserDao userService = new UserDao();
            //User x = null;
            //Make the call to the 'Model' by using the UserService class to get all Users...
            try{
            List<User> students = userService.listAllStudents();
            
            //Put the list of users into the session so that JSP(the View) can pick them up & display them...
            session.setAttribute("students", students);
            }
            catch(DaoException e){
                e.printStackTrace();
            }
            forwardToJsp = "Admin.jsp";
        }
        return forwardToJsp;
    } 
}
