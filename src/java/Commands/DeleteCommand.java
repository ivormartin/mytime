/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Commands;

import Exceptions.DaoException;
import School.*;
import SchoolDao.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class DeleteCommand implements Command{
    
    public String execute(HttpServletRequest request,HttpServletResponse response){
        System.out.println("inside delete command");
        String forwardToJsp = "";
        HttpSession session = request.getSession();
        int id = (Integer)(session.getAttribute("theStuId"));
        boolean hasLessonsLogged = false;
        System.out.println("id of student to be deleted " +id);
        User theUser = null;
        String message = "";
        int removed = 0;    
        UserDao ud = new UserDao();
        LessonDao ld = new LessonDao();
        try{
            theUser = ud.findUserById(id);
            hasLessonsLogged = ld.findLessonByStuId(id);
            if(theUser!=null){
                message = "Student: "+theUser.getFname()+" has been removed";
            }
            System.out.println(theUser);
            removed = ud.removeStudent(theUser);
            if(removed>0){
                session.setAttribute("message", message);
                session.removeAttribute("student");
                session.removeAttribute("theStuId");
            }
           
            System.out.println("removed number is "+removed);
            forwardToJsp = "Admin.jsp";
        }
        catch(SQLException e){
            e.printStackTrace();
        }
     return forwardToJsp;
    }
    
}
