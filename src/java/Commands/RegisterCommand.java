/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Commands;
import Exceptions.DaoException;
import School.*;
import SchoolDao.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/*
 * @author user
 */
public class RegisterCommand implements Command{
    
    public String execute(HttpServletRequest request, HttpServletResponse response){
            HttpSession session = request.getSession();
            String id = (String)session.getAttribute("loggedSessionId");
                String forwardToJsp = "";
                System.out.println("On registration page");
             //if the student is not logged in then go ahead and register 
                
            if(id==null){
                System.out.println("in here cause sessionId is null"+id);
                UserDao userService = new UserDao();
                System.out.println("making new student here");
                User theUserLoggedIn;
                String un = request.getParameter("un");
                String pw = request.getParameter("pw");
                String fn = request.getParameter("fname");
                String ln = request.getParameter("lname");
                String em = request.getParameter("email");
                int cid = Integer.parseInt(request.getParameter("classid"));
                String bool = request.getParameter("bool");
                boolean typeOf = getUserType(bool);
                theUserLoggedIn = new User(0,un,pw,fn,ln,em,cid,typeOf);
                
               try{
                    boolean studentRegistered = userService.register(theUserLoggedIn);
                    System.out.println("boolean studentRegistered is "+studentRegistered);
                 if(studentRegistered==true){  
                     session.setAttribute("newUser",theUserLoggedIn);
                     System.out.println("The full name of the newly registered student is "+theUserLoggedIn.getFname()+" "+theUserLoggedIn.getLname());
                     forwardToJsp = "logIn.jsp";
                  }else{
                     System.out.println("Login failed not registered so i here");
                     forwardToJsp = "Register.jsp";
                 }
               }
               catch(DaoException e){
                   e.printStackTrace();
               }
         }else{
                forwardToJsp = "logIn.jsp";
            }
           return forwardToJsp; 
        }

/*****************      determine either student or teacher before registration      ***********************/    
    
    public boolean getUserType(String typeOf){
    boolean userType = false;
    if(typeOf.equalsIgnoreCase("teacher")){
           userType = true;
     } 
        return userType;
    }       

    }

