package Commands;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutCommand implements Command 
{
    public String execute(HttpServletRequest request, HttpServletResponse response) 
    {
        // Get the session object and invalidate it - this removes all data stored up to now 
        // (resets it to blank)
        HttpSession session = request.getSession();
        session.invalidate();
        
        // Send the user back to the login page
        String forwardToJsp = "logIn.jsp";
        
        return forwardToJsp;
    }
}