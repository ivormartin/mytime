package Commands;
//import Servlets.logInServlet;

public class CommandFactory {

    private static CommandFactory factory = null;
    
    private CommandFactory() {
    }
	
    public static synchronized CommandFactory getInstance()
    {
        if(factory == null)
        {
            factory = new CommandFactory();
        }
        return factory;
    }
    
    public synchronized Command createCommand(String commandStr) 
    {
    	Command command = null;
    	
	//Instantiate the required Command object...
    	if (commandStr.equals("logIn")) {
    		command = new LoginCommand();
    	}
    	if (commandStr.equals("register")) {
    		command = new RegisterCommand();
    	}
        if(commandStr.equals("lesson")){
                command = new LessonCommand();
        }
        if(commandStr.equals("list")){
                command = new ListCommand();
        }
        if(commandStr.equals("view")){
                command = new ViewCommand();
        }
         if(commandStr.equals("delete")){
                command = new DeleteCommand();
        }
         if(commandStr.equals("edit")){
                command = new EditCommand();
        }
         if(commandStr.equals("create")){
                command = new CreateCommand();
        }
         if(commandStr.equals("logout")){
                command = new LogoutCommand();
        } 
         if(commandStr.equals("mongo")){
                command = new ListMongoDocsCommand();
        } 
         if(commandStr.equals("deletemongodoc")){
                command = new DeleteMongoDocCommand();
        }
    	//Return the instantiated Command object to the calling code...
    	return command;// may be null
    }    
}
