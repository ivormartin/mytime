/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Commands;

import School.User;
import SchoolDao.UserDao;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 *
 * @author user
 */
public class EditCommand implements Command{
    
    public String execute(HttpServletRequest request,HttpServletResponse response){
        
        System.out.println("inside edit command");
        String forwardToJsp = "Admin.jsp";
        UserDao ud = new UserDao();
        HttpSession session = request.getSession();
        User oldUser = null;
        String username = "";
        String password = "";
        String firstname = "";
        String lastname = "";
        String email = "";
        int classId = 0;
        username = request.getParameter("un");
        password = request.getParameter("pw");
        firstname = request.getParameter("fname");
        lastname = request.getParameter("lname");
        email = request.getParameter("email");
        classId = Integer.parseInt(request.getParameter("classid"));
        int id = (Integer)(session.getAttribute("theStuId")); 
        try{
        oldUser = ud.findUserById(id);
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        System.out.println("stu to be edit is "+id);
        boolean isTeacher = false;
        User upDatedStudent = new User(id,username,password,firstname,lastname,email,classId,isTeacher);
        
        System.out.println("newuser details are "+upDatedStudent);
        System.out.println("id of student to be edited " +id);
        //User theUser = null;
        String message = "";
        int edited = 0;    
        
        try{
            edited = ud.editStudent(upDatedStudent);
           
            if(edited>0){
                message = "Student: "+upDatedStudent.getFname()+" has had their details updated";
                //session.removeAttribute("theId");
                session.setAttribute("message", message);
                session.removeAttribute("student");
            }
            System.out.println("the edit number is "+edited);

            //forwardToJsp = "Admin.jsp";
        }
        catch(SQLException e){
            e.printStackTrace();
        }
     return forwardToJsp;
    }
    
    
}
