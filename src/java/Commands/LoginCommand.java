/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Commands;
import Exceptions.*;
import MongodbPackage.MongoConnection;
import School.*;
import School.Lesson;
import SchoolDao.*;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.RollingFileAppender;
import org.apache.log4j.SimpleLayout;
/**
 * @author user
 */
public class LoginCommand extends MongoConnection implements Command{
    
     final Logger logger = Logger.getLogger(this.getClass());
     
     DB db;
     DBCollection coll;
     BasicDBObject doc;
     DBCursor cursor;
     DBObject updateDocument;
     public String execute(HttpServletRequest request, HttpServletResponse response){
        String message;
        try{
            db = getMongoConnection();
            coll = db.getCollection("logStore");
            cursor = coll.find();
            updateDocument = cursor.next();
            
        }catch(UnknownHostException e){
	     System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        RollingFileAppender a0 = null;
        String forwardToJsp;
        //System.out.println("On login page");
        String myun = request.getParameter("un");
        String mypw = request.getParameter("pw");
        String loginFailedMessage = "";
        HttpSession session = request.getSession();
        Date nd = new Date(System.currentTimeMillis());
        try{
        a0 = new RollingFileAppender(new SimpleLayout(),"login");
        
         logger.addAppender(a0);
        }catch(IOException e){
            e.getMessage();
        }
            if ( myun != "" && mypw != "" ) 
            {
                //Use the UserDao class to login...
                UserDao studentService = new UserDao();
                User theUserLoggedIn = studentService.logIn(myun,mypw);
                //HttpSession session = request.getSession();
                if (theUserLoggedIn != null)
                {    
                   message = theUserLoggedIn+" has logged in at "+ nd;
                    logger.info(message);
                    doc = new BasicDBObject("loglevel", "Info Message").append("message", message);
                    coll.insert(doc);
                //HttpSession session = request.getSession();
                    String clientSessionId = session.getId();
                    //If login successful, store the session id for this client... 
                    session.setAttribute("loggedSessionId", clientSessionId);
                    //also store the user
                    session.setAttribute("theUser",theUserLoggedIn);
                    //System.out.println("Only here cause logged in, studentLoggingIn is "+theUserLoggedIn);

                    forwardToJsp = "logIn.jsp";				
                }
                else
                {
                    message = "Incorrect details at login attempt  "+ nd;
                    logger.error(message);
                    doc = new BasicDBObject("loglevel", "Error Message").append("message", message);
                    coll.insert(doc);
                    //updateDocument.put("log.error", message);
                    
                    //System.out.println("Student object after login attempt is "+theUserLoggedIn);
                    //System.out.println("Here cause login attempt failed need to try again");
                    loginFailedMessage = "Your username and password "+"\n"+" combination don't match any on the system";
                    forwardToJsp = "Register.jsp";
                    session.setAttribute("failedLoginMessage",loginFailedMessage);
                }
            }
            else
            {
                message = "Nothing entered at login attempt "+nd;
                logger.error(message);
                doc = new BasicDBObject("loglevel", "Error Message").append("message", message);
                coll.insert(doc);
                //System.out.println("In here cause no username or password entered");
                loginFailedMessage = "You didn't enter either a username "+"\n"+" or password, register or retry";
                forwardToJsp = "Register.jsp";	
                session.setAttribute("failedLoginMessage",loginFailedMessage);
            }
            logger.removeAppender("loginCommand");
            /*cursor = coll.find();
                    while(cursor.hasNext()){
                        System.out.println(" Doc");
                        System.out.println(cursor.next());
                    }*/
        return forwardToJsp;
    }
}