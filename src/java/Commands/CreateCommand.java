/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commands;
import School.User;
import SchoolDao.UserDao;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.apache.log4j.RollingFileAppender;
import org.apache.log4j.SimpleLayout;
/*
 * @author user
 */
public class CreateCommand implements Command{
    
    final Logger logg = Logger.getLogger(this.getClass());
    
    public String execute(HttpServletRequest request,HttpServletResponse response){
        RollingFileAppender rfa = null;
        try {
            rfa = new RollingFileAppender(new SimpleLayout(),"create");
            logg.addAppender(rfa);
        } catch (IOException ex) {
            ex.getMessage();
        }
        System.out.println("inside create command");
        String forwardToJsp = "Admin.jsp";
        UserDao ud = new UserDao();
        HttpSession session = request.getSession();

        boolean isTeacher = false;
        String username = request.getParameter("un");
        String password = request.getParameter("pw");
        String firstname = request.getParameter("fname");
        String lastname = request.getParameter("lname");
        String email = request.getParameter("email");
        int classID = Integer.parseInt(request.getParameter("classid"));
        int id = 0;
        User upDatedStudent = new User(id,username,password,firstname,lastname,email,classID,isTeacher);
        System.out.println("newuser details are "+upDatedStudent);

        String message = "";
        boolean isCreated;
        try{
            isCreated = ud.register(upDatedStudent);
           
            if(isCreated){
                message = "Student: "+upDatedStudent.getFname()+" has been registered";
                logg.info("\n"+message);
                
                session.setAttribute("message", message);
                session.removeAttribute("student");
            }
            System.out.println("student created is "+isCreated);
        }
        catch(SQLException e){
            e.printStackTrace();
        }
     return forwardToJsp;
    }
}