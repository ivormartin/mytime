/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Commands;

import SchoolDao.*;
import School.*;
import Exceptions.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class ViewCommand implements Command{
    
    public String execute(HttpServletRequest request,HttpServletResponse response){
        System.out.println("in the view command");
        String forwardToJsp = "";
        int stuid = 0;
        HttpSession session = request.getSession();
        stuid = Integer.parseInt(request.getParameter("theId"));
      
        session.setAttribute("theStuId", stuid);
     
        System.out.println("student id in viewcommand is "+stuid);
        
       if(stuid!=0){
            UserDao ud = new UserDao();  
            User student = null;
            try{
                System.out.println("userId is "+ stuid);
                student = ud.findUserById(stuid);
                System.out.println(student);
                //String theStuId = (String)student.getUserId();
                //request.setParameter("theId",student.getUserId());
                session.setAttribute("student",student);
                forwardToJsp = "Admin.jsp";
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
         return forwardToJsp;
    }
}