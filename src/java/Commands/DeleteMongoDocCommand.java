/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Commands;
import Exceptions.DaoException;
import MongodbPackage.MongoDocs;
import School.User;
import SchoolDao.UserDao;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 *
 * @author user
 */
public class DeleteMongoDocCommand implements Command{
    
    public String execute(HttpServletRequest request, HttpServletResponse response) 
    {
        //Check to see if the session id coming from the client matches the id stored at login...
        HttpSession session = request.getSession();
        String forwardToJsp;

        //User not logged in...
        if ( session.getId() != session.getAttribute("loggedSessionId") )
        {
            forwardToJsp = "logIn.jsp";
        }
        else
        {	
            MongoDocs getDocs = new MongoDocs();
            getDocs.removeDoc();
           
            forwardToJsp = "LogFiles.jsp";
        }
        return forwardToJsp;
    } 
    
}
