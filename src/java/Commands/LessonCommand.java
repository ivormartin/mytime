/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Commands;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import School.*;
import SchoolDao.*;
import Exceptions.*;
/*
 * @author user
 */
public class LessonCommand implements Command{
    
    public String execute(HttpServletRequest request, HttpServletResponse response){
        System.out.println("This is the lessonCommand execute function");
        HttpSession session = request.getSession();
        User theStudent = (User) session.getAttribute("theUser");
        String forwardToJsp = "TimeLesson.jsp";
        String message="";
        boolean recorded = false;
        String lessonName = ""; 
        int tally = 10;

        System.out.println(theStudent);
    
          int  right = Integer.parseInt(request.getParameter("ok"));
          int  wrong = Integer.parseInt(request.getParameter("xx"));
             System.out.println(right);
             System.out.println(wrong);
          int total = right + wrong;   
         if(total == tally){
             try{
                LessonDao lessonService = new LessonDao();
                lessonName = request.getParameter("lessonOpt");
                Lesson theLesson = lessonService.findLessonByName(lessonName);
                System.out.println(theLesson);

                recorded = lessonService.recordLesson(theStudent,theLesson,right);
                    if(recorded){
                        message = "Lesson successfully recorded";
                        session.setAttribute("lessonMessage", message);
                    }
             }
            catch(DaoException e){
                e.printStackTrace();
            }
         }else if(total<tally){
                message = total+" attempts made,\n "+(tally-total)+" more required,\n please continue";
                session.setAttribute("lessonMessage", message);
         }else{
                forwardToJsp = "TimeLesson.jsp";
         }
        return forwardToJsp;
    }
    
}
 