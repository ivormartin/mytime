/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SchoolDao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import Exceptions.DaoException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class Dao 
{
    
    private DataSource datasource;
    
    public Dao(DataSource ds){
        this.datasource = ds;    
    }
    
    public Dao(){
        //Connection con = null;
        String DATASOURCE_CONTEXT = "jdbc/myTimeResource";
        try{
            Context initialContext = new InitialContext();
            DataSource ds = (DataSource)initialContext.lookup("java:comp/env/"+DATASOURCE_CONTEXT);
            if(ds!=null){
                datasource = ds;
            }else{
                System.out.println("Failed to lookup datasource.");
            }
        }catch(NamingException ex){
            System.out.println("Cannot get connection: "+ex);
        }
        
    }
    
public Connection getConnection() throws DaoException
{
        Connection conn = null;
        try{
            if (datasource != null) {
                	conn = datasource.getConnection();
            }else {
                	System.out.println(("Failed to lookup datasource."));
            }
        }catch (SQLException ex2){
            	System.out.println("Connection failed " + ex2.getMessage());
            	System.exit(2); // Abnormal termination
        }
        return conn;
}


    public void freeConnection(Connection con) throws DaoException 
    {
        try 
        {
            if (con != null) 
            {
                con.close();
                con = null;
            }
        } 
        catch (SQLException e) 
        {
            System.out.println("Failed to free connection: " + e.getMessage());
            System.exit(1);
        }
    }   
}
