/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SchoolDao;

import School.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/*
 *
 * @author user
 */
public class SchoolDao extends Dao{
    
   public User findSchoolById(int id)throws SQLException
  {
     Connection con = null;
     PreparedStatement ps = null;
     ResultSet rs = null;
     User student = null;
     try {
            con = this.getConnection();
            String query = "SELECT * FROM school WHERE schoolId = ?";
            ps = con.prepareStatement(query);
            ps.setInt(1,id);
            rs = ps.executeQuery();
           if(rs.next())
            {
               student = new User(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getInt(7),rs.getBoolean(8));
            }
        } catch (SQLException e) {
           System.out.println("logIn(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
               
            } catch (SQLException e) {
               System.out.println("logIn(): " + e.getMessage());
            }
        }
     return student;
 }
   
}