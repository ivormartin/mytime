/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SchoolDao;

import Exceptions.DaoException;
import School.Lesson;
import School.User;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author user
 */
public interface UsersInterface {
    
    public boolean register(User s)throws DaoException;
    public User logIn(String uname,String pword);
    public List<User> listAllStudents() throws DaoException;
    public User findUserById(int id)throws SQLException;
    public int removeStudent(User student)throws SQLException;
    public int editStudent(User student)throws SQLException;
}
