/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SchoolDao;

import Exceptions.DaoException;
import School.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
public class AjaxDao extends Dao{
    
    public String checkUsernames(String un) throws DaoException 
  {
    	Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String isAllowed = "";
        
        try 
        {
            System.out.println("inside checkUsernames in AjaxDao");
            //Get connection object using the methods in the super class (Dao.java)...
            con = this.getConnection();
            System.out.println(un);
            String query = "SELECT uname FROM allusers WHERE uname LIKE ?";
            ps = con.prepareStatement(query);
            ps.setString(1,un);
            rs = ps.executeQuery();
            if(rs.next()) 
            {
              isAllowed = "denied";
           
            }else{
              isAllowed = "okay";  
            }
            System.out.println(isAllowed);
        }
        
        catch (SQLException e) 
        {
            throw new DaoException("checkUsernames() " + e.getMessage());
        } 
        finally 
        {
            try 
            {
                if (rs != null) 
                {
                    rs.close();
                }
                if (ps != null) 
                {
                    ps.close();
                }
                if (con != null) 
                {
                    freeConnection(con);
                }
            } 
            catch (SQLException e) 
            {
                throw new DaoException(e.getMessage());
            }
        }
        return isAllowed;
    }
}
