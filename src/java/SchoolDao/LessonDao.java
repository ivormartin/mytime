/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SchoolDao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import School.*;
import Exceptions.DaoException;
import java.util.GregorianCalendar;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
/*
 * @author user
 */
public class LessonDao extends Dao implements LessonsInterface{
    
     public boolean recordLesson(User u,Lesson l,int sc) throws DaoException{
       Connection con = null;
       PreparedStatement ps = null;
       ResultSet rs = null;
       boolean isInserted = false;
       int i = 0;
       GregorianCalendar gregCal = new GregorianCalendar();
       long ms = gregCal.getTimeInMillis();
       System.out.println("In here to make a stu_lesson record");
      try{
      con = this.getConnection();
      con.setAutoCommit(false);
      con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
      String command = "INSERT INTO stu_lesson (lessonId, stuId, score, lesson_date)"
                    +" VALUES(?,?,?,?)";
            ps = con.prepareStatement(command);
            ps.setInt(1, l.getLessonId());
            ps.setInt(2, u.getUserId());
            ps.setInt(3, sc);
            ps.setDate(4, (new Date(ms)));
       //transaction stops user logging score of 0     
       
        i= ps.executeUpdate();
        if(i>0){
            isInserted = true;
        }
        if(sc==0){
           con.rollback();
       }else{
           con.commit();
       }
    } catch (SQLException e) {
            throw new DaoException("recordLesson(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("recordLesson(): " + e.getMessage());
            }
        }
    return isInserted;
 }
    
     public List<Lesson> findAllLessons() throws DaoException 
    {
    	Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Lesson> lessons = new ArrayList<Lesson>();
        try 
        {
            //Get connection object using the methods in the super class (Dao.java)...
            con = this.getConnection();

            String query = "SELECT * FROM lesson";
            ps = con.prepareStatement(query);
            
            //Using a PreparedStatement to execute SQL...
            rs = ps.executeQuery();
            while (rs.next()) 
            {
                int lessonId = rs.getInt("lessonId");
                String lessonName = rs.getString("lesson_name");

                Lesson ls = new Lesson(lessonId, lessonName);
                lessons.add(ls);
            }
        } 
        catch (SQLException e) 
        {
            throw new DaoException("findAllLessons() " + e.getMessage());
        } 
        finally 
        {
            try 
            {
                if (rs != null) 
                {
                    rs.close();
                }
                if (ps != null) 
                {
                    ps.close();
                }
                if (con != null) 
                {
                    freeConnection(con);
                }
            } 
            catch (SQLException e) 
            {
                throw new DaoException(e.getMessage());
            }
        }
        return lessons;     // may be empty
    }
    
/***********       get lesson object from dropdownlist lesson name    ***********/
     
    public Lesson findLessonByName(String lessonName) throws DaoException 
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Lesson ln = null;
        try {
            con = this.getConnection();
            
            String query = "SELECT * FROM lesson WHERE lesson_name = ?";
                    
            ps = con.prepareStatement(query);
            ps.setString(1, lessonName);
            
            rs = ps.executeQuery();
            if (rs.next()) 
            {
            	int lesId = rs.getInt("lessonId");
                String lesName = rs.getString("lesson_name");

                ln = new Lesson(lesId, lesName);
            }
        } 
        catch (SQLException e) 
        {
            throw new DaoException("findLessonByName " + e.getMessage());
        } 
        finally 
        {
            try 
            {
                if (rs != null) 
                {
                    rs.close();
                }
                if (ps != null) 
                {
                    ps.close();
                }
                if (con != null) 
                {
                    freeConnection(con);
                }
            } 
            catch (SQLException e) 
            {
                throw new DaoException("findLessonByName" + e.getMessage());
            }
        }
        return ln;     // ln may be null 
    }
    
         public List<Lesson> lessonsByCounty() throws DaoException 
    {
    	Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Lesson> lessons = new ArrayList<Lesson>();
        try 
        {
            //Get connection object using the methods in the super class (Dao.java)...
            con = this.getConnection();

            String query = "SELECT score, FROM StuLesson, GROUP BY county";
            ps = con.prepareStatement(query);
            
            //Using a PreparedStatement to execute SQL...
            rs = ps.executeQuery();
            while (rs.next()) 
            {
                int lessonId = rs.getInt("lessonId");
                String lessonName = rs.getString("lesson_name");

                Lesson ls = new Lesson(lessonId, lessonName);
                lessons.add(ls);
            }
        } 
        catch (SQLException e) 
        {
            throw new DaoException("findAllLessons() " + e.getMessage());
        } 
        finally 
        {
            try 
            {
                if (rs != null) 
                {
                    rs.close();
                }
                if (ps != null) 
                {
                    ps.close();
                }
                if (con != null) 
                {
                    freeConnection(con);
                }
            } 
            catch (SQLException e) 
            {
                throw new DaoException(e.getMessage());
            }
        }
        return lessons;     // may be empty
    }

       public boolean findLessonByStuId(int stuid) throws DaoException 
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean studentLesson = false;
        try {
            con = this.getConnection();
            con.setAutoCommit(false);
            
            String query = "SELECT * FROM stu_lesson WHERE stuId = ?";
                    
            ps = con.prepareStatement(query);
            ps.setInt(1, stuid);
            
            rs = ps.executeQuery();
            if (rs.next()) 
            {
            	studentLesson = true;
            }
        } 
        catch (SQLException e) 
        {
            throw new DaoException("findLessonByStuId " + e.getMessage());
        } 
        finally 
        {
            try 
            {
                if (rs != null) 
                {
                    rs.close();
                }
                if (ps != null) 
                {
                    ps.close();
                }
                if (con != null) 
                {
                    freeConnection(con);
                }
            } 
            catch (SQLException e) 
            {
                throw new DaoException("findLessonByStuId" + e.getMessage());
            }
        }
        return studentLesson;    //has the student lessons logged
    }      
         
}