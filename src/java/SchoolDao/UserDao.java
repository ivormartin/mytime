/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SchoolDao;
import Exceptions.DaoException;
import School.User;
import SchoolDao.Dao;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import javax.sql.DataSource;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;

public class UserDao extends Dao implements UsersInterface{
 
    public UserDao(DataSource ds){
        super(ds);
    }
    
    public UserDao(){
        super();
    }
    
 public User logIn(String uname,String pword)
         
  {
     
     Connection con = null;
     PreparedStatement ps = null;
     ResultSet rs = null;
     User s = null;
     try {
            con = this.getConnection();

            String query = "SELECT * FROM allusers WHERE uname = ? AND pword = ?";
            ps = con.prepareStatement(query);
            ps.setString(1,uname);
            ps.setString(2,pword);
            rs = ps.executeQuery();
            
           if(rs.next())
            {
           
               s = new User(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getInt(7),rs.getBoolean(8));
            }

        } catch (SQLException e) {
           System.out.println("logIn(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
               
            } catch (SQLException e) {
               System.out.println("logIn(): " + e.getMessage());
            }
        }
     return s;
 }

/***************        Register as a user         ***************************/
 
 public boolean register(User s)throws DaoException
  {
    Connection con = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    boolean isCreated = false;
    int i = 0;
    
    try{
      con = this.getConnection();
      
      String query = "SELECT uname,pword FROM allusers WHERE uname = ? AND pword = ?";
      ps = con.prepareStatement(query);       //check if user is already in db
      ps.setString(1,s.getUname());
      ps.setString(2,s.getPword());
      rs = ps.executeQuery();
        if (rs.next()) {
            throw new DaoException("Username " + s.getUname() +" and Password "+s.getPword()+ " already exists");
        }
      String command = "INSERT INTO allusers (userId,uname, pword, fname, lname,"
                    + "email,classID,isTeacher) VALUES(?,?,?,?,?,?,?,?)";
            ps = con.prepareStatement(command);
            ps.setInt(1,s.getUserId());
            ps.setString(2, s.getUname());
            ps.setString(3, s.getPword());
            ps.setString(4, s.getFname());
            ps.setString(5, s.getLname());
            ps.setString(6, s.getEmail());
            ps.setInt(7, s.getClassID());
            ps.setBoolean(8, s.getType());
            
        i= ps.executeUpdate();
        if(i>0){
            isCreated = true;
        }
    } catch (SQLException e) {
            throw new DaoException("register(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("register(): " + e.getMessage());
            }
        }
    return isCreated;
 }

/***************       List only the Students     ****************************/ 
 
 public List<User> listAllStudents() throws DaoException 
  {
    	Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<User> students = new ArrayList<User>();
        
        try 
        {
            //Get connection object using the methods in the super class (Dao.java)...
            con = this.getConnection();
            String query = "SELECT * FROM allusers";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) 
            {
                
                int userId = rs.getInt(1);
                String username = rs.getString(2);
                String password = rs.getString(3);
                String firstname = rs.getString(4);
                String lastname = rs.getString(5);
                String email = rs.getString(6);
                int classID = rs.getInt(7);
                boolean isTeacher = rs.getBoolean(8);
                //only add students to the list
                if(!isTeacher){
                User student = new User(userId,username,password,firstname,lastname,email,classID,isTeacher);
                students.add(student);
                }
            }
        }
        catch (SQLException e) 
        {
            throw new DaoException("listAllUsers() " + e.getMessage());
        } 
        finally 
        {
            try 
            {
                if (rs != null) 
                {
                    rs.close();
                }
                if (ps != null) 
                {
                    ps.close();
                }
                if (con != null) 
                {
                    freeConnection(con);
                }
            } 
            catch (SQLException e) 
            {
                throw new DaoException(e.getMessage());
            }
        }
        return students;     // may be empty
    }
 
/******************      Find User By ID        ******************************/
 
 public User findUserById(int id)throws SQLException
  {
     
     Connection con = null;
     PreparedStatement ps = null;
     ResultSet rs = null;
     User student = null;
     try {  
            con = this.getConnection();
            String query = "SELECT * FROM allusers WHERE userId = ?";
            ps = con.prepareStatement(query);
            ps.setInt(1,id);
            rs = ps.executeQuery();
           if(rs.next())
            {
               student = new User(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getInt(7),rs.getBoolean(8));
            }

        } catch (SQLException e) {
           System.out.println("logIn(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
               
            } catch (SQLException e) {
               System.out.println("logIn(): " + e.getMessage());
            }
        }
     return student;
 } 

 /****************     RemoveStudent method     ******************************/
 
 public int removeStudent(User student)throws SQLException
  {
     System.out.println("here in the remove method");
     Connection con = null;
     PreparedStatement ps = null;
     ResultSet rs = null;
     int id = student.getUserId();
     int isDeleted = 0;
     try {
            con = this.getConnection();
            //con.setAutoCommit(false);
            String command = "DELETE FROM allusers WHERE userId = ?";
            
            ps = con.prepareStatement(command);
            ps.setInt(1,id);
            isDeleted = ps.executeUpdate();
           
        } catch (SQLException e) {
           System.out.println("removeStudent(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
               System.out.println("removeStudent(): " + e.getMessage());
            }
        }
     return isDeleted;
 } 
 
 /****************     Edit Student Details         **************************/
 
 public int editStudent(User student)throws SQLException
  {
     
     System.out.println("here in the edit method");
     Connection con = null;
     PreparedStatement ps = null;
     ResultSet rs = null;
     int id = student.getUserId();
     //int classid = student.getClassID();
     int isEdited = 0;
     System.out.println("stu id in edit method is "+id);

     try {
            con = this.getConnection();
            String command = "UPDATE allusers SET uname = ?,"
                    + "pword = ?,"
                    + "fname = ?,"
                    + "lname = ?,"
                    + "email = ?"
                    + "WHERE userId = ?";
            ps = con.prepareStatement(command);
            ps.setString(1,student.getUname());
            ps.setString(2,student.getPword());
            ps.setString(3,student.getFname());
            ps.setString(4,student.getLname());
            ps.setString(5,student.getEmail());
            
            ps.setInt(6,id);
            isEdited = ps.executeUpdate();
            
        } catch (SQLException e) {
           System.out.println("editStudent(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
               System.out.println("editStudent(): " + e.getMessage());
            }
        }
     return isEdited;
 }
 
}
