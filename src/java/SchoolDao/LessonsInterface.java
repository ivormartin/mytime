/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SchoolDao;

import Exceptions.DaoException;
import School.Lesson;
import School.User;
import java.util.List;

/**
 *
 * @author user
 */
public interface LessonsInterface {
    public boolean recordLesson(User u,Lesson l,int sc)throws DaoException;
    public List<Lesson> findAllLessons()throws DaoException;
    public Lesson findLessonByName(String lessonName)throws DaoException;
}
