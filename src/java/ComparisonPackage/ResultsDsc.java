/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComparisonPackage;
import School.StuLesson;
import java.util.Comparator;
/*
 * @author user
 */
public class ResultsDsc implements Comparator{
    
        public int compare(Object obj1,Object obj2){
            StuLesson c1 = (StuLesson)obj1;
            StuLesson c2 = (StuLesson)obj2;
            
            if(c1.getScore() > c2.getScore())
                return -1;
            if(c1.getScore() < c2.getScore())
                return 1;
        return 0;
        }
}