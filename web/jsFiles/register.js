//valid is a Global boolean variable used by the various validation functions below
var valid=false;

/*******************************************************
 * *initPage and its associated functions adapted from*
 * Head First Ajax                                     *
 * Accessed 27/02/2014  Safari Books Library Dkit      *
 * *****************************************************/

window.onload = initPage;

function initPage(){
    document.getElementById("un").onblur = checkUsername;
    document.getElementById("reg").onsubmit = validateForm;
    //check username before sending form
    //document.getElementById("reg").disabled = true;
   
}

function checkUsername(){
    
    document.getElementById("un").className = "thinking";
    request = createRequest();
    if(request == null){
        alert("Unable to create request");
    }else{
        var theName = document.getElementById("un").value;
        var username = escape(theName);
        var url = "PageContentServlet?action=username&content="+username;
        request.onreadystatechange = showUsernameStatus;
        request.open("GET",url,true);        
        request.send(null);
    }
}

function showUsernameStatus(){
    
   if(request.readyState == 4){
       if(request.status == 200){
           
           if(request.responseText == "okay"){
               
               //alert(request.responseText);
               document.getElementById("un").className = "approved";
               document.getElementById("reg").disabled = false;
           }
           else{
               //if problem alert user
               document.getElementById("un").className = "denied";
               document.getElementById("un").value = "Username taken";
               document.getElementById("un").focus();
               document.getElementById("un").select();
               document.getElementById("reg").disabled = true;
           }
       }
   } 
}

/*******************************************************************************/

function validateForm()
{

var valid = true;	
if((document.reg.un.value === "") || (!isNaN(document.reg.un.value)) || (document.reg.un.value === null))
        {
           document.forms["reg"].elements["un"].value = "not valid";  
           document.reg.un.focus();
           document.forms["reg"].elements["un"].className = "denied";
		valid = false;
        }
if((document.reg.pw.value === "") || (document.reg.pw.value === null) || (!isNaN(document.reg.pw.value)))
        {
           document.forms["reg"].elements["pw"].value = "not valid"; 
           document.forms["reg"].elements["pw"].className = "denied"; 
           document.reg.pw.focus();
		valid = false;
        }
if (( document.reg.fname.value === "") || (document.reg.fname.value === null) || (!isNaN(document.reg.fname.value)))
        {
           document.forms["reg"].elements["fname"].className = "denied";
           document.forms["reg"].elements["fname"].value = "not valid";
           document.reg.fname.focus();
		valid = false;
        }
if ((document.reg.lname.value === "") || (document.reg.lname.value === null) || (!isNaN(document.reg.lname.value)))
        {
           document.forms["reg"].elements["lname"].className = "denied"; 
           document.forms["reg"].elements["lname"].value = "not valid";
           document.reg.lname.focus();
                valid = false;
        }	
if ((document.reg.email.value === "")||(document.reg.email.value === null) || (!isNaN(document.reg.email.value)))
        {
           document.forms["reg"].elements["email"].className = "denied"; 
           document.forms["reg"].elements["email"].value = "not valid";
           document.reg.email.focus();
		valid = false;
        }
		else
		{
			ValidateEmail(email);	
		}
if ((document.reg.classid.value === "") || (document.reg.classid.value === null) || (isNaN(document.reg.classid.value)))                
        { 
            document.forms["reg"].elements["classid"].value = "not valid";
            document.forms["reg"].elements["classid"].className = "denied";
            document.reg.classid.focus();
                    valid = false;
        }
if ((document.reg.bool.value === "") ||(document.reg.bool.value === null) || (!isNaN(document.reg.bool.value)))                
        { 
            document.forms["reg"].elements["bool"].value = "not valid"; 
            document.forms["reg"].elements["bool"].className = "denied";
            document.reg.bool.focus();
                    valid = false;
        }	
                            return valid;
}

/******************************************************************************/

function nameCheck()
{
 if (document.reg.name.value === "" || isNumeric(document.reg.name.value))
        {
                document.reg.name.value = "Please enter name";
        }
}

function emailCheck()
{
if (document.reg.email.value === "") 
	{
			
		email.focus()
	}	
}

function ValidateEmail(inputText)
{
    var mailformat =/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(inputText.value.match(mailformat))
{

    return true;
}
else
{
   
    document.reg.email.value = "";
    document.reg.email.focus();
return false;
}
}

function registerStudent(){
    
    //return validateForm('reg');
    if(validateForm("reg"))
    {
        //alert("fields ok");
        document.getElementById("reg").disabled = false;
        document.getElementById("regStudent").value = "register";
        document.getElementById("reg").submit();
    }else{
        //alert("fields not ok");
        document.getElementById("reg").disabled = true;  
    }   
}


/*function loadXMLDoc()
{
var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","ajax_info.txt",true);
xmlhttp.send();
}*/


/*
function getCookieInfo(){
	var cInfo = document.cookie;
	cInfo = cInfo.substring(cInfo.lastIndexOf("FeedBackCookie"));
	cInfo = cInfo.substring(cInfo.indexOf("=")+1);
	var cookieContents = new Array(4);
	var start = 0;
	var finish = cInfo.indexOf("%",0);
	//alert("FeedBackCookie cookie is:\n\n "+cInfo);
	for(var i = 0; i<cookieContents.length; i++) {
		cookieContents[i] = cInfo.substring(start,finish);
		//alert("cookieContents["+i+"]= "+cookieContents[i]+"; start= "+start+"; finish= "+finish);
		start = finish + 1;
		finish = cInfo.indexOf("%",start);
	}
	
	document.getElementById("tyName").value = "Hi "+cookieContents[0];
	document.getElementById("tyEmail").value = "We will Email you at: "+cookieContents[1];
	document.getElementById("tyPurchase").value = "You join us "+cookieContents[3]+" times a week";
	document.getElementById("tyService").value = "You rated our service as "+cookieContents[2];
	*/

