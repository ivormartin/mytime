/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var newMins;        //use to find out what random value was chosen for the mins
var newHr;          //use to find what random value was chosen for the hrs
   
   
function setupClock() {
    
 var canvas = document.getElementById('myCanvas');
    var w = parseInt(canvas.getAttribute('width'));
    var h = parseInt(canvas.getAttribute('height'));
    var context = canvas.getContext('2d');

    
    context.translate(w/2, h/2);
    createClock();
   
   
function degreesToRadians(degrees) {
    return (Math.PI / 180) * degrees;
}

 function drawHourHand(hr){
     
     hr.hours = Math.floor((Math.random()*12)+1);
     newHr = hr.hours;
     hr.getHrs = function() {return hr.hours;};
     var hrs = (hr.getHrs()*30);
     context.save();
     context.fillStyle = 'red';
     context.rotate(degreesToRadians(hrs));
     drawHand(140,7);
     context.restore();
 }

 function drawMinuteHand(mn){   
    mn.minutes = Math.floor((Math.random()*12)+1);
    newMins = mn.minutes*5;
    mn.getMins = function()   {return mn.minutes;};
    var mins = (mn.getMins()*30);
    context.save();
    context.fillStyle = "RGB(165,0,43)";
    context.rotate( degreesToRadians(mins));
    drawHand(170,8);
    context.restore();
 }
 
 
function setTimeValues(){   //put the random min/hr values into text boxes for use when analysing user input
    document.getElementById('minVal').value = newMins;
    document.getElementById('hrVal').value = newHr;
}

function drawHand(size,thickness){
   thickness = thickness || 4;
   context.beginPath();
   context.moveTo(0,0); // start at center  the following is a closed path
   context.lineTo(thickness*-1, -10);//value left, 10 up
   context.lineTo(0, size * -1);     //up to 0,-1
   context.lineTo(thickness,-10);   //down value right,10 up
   context.lineTo(0,0);             //back to center
   context.fill();
 }

 function createClock(){
   var mn = new Object();
   var hr = new Object();
   drawMinuteHand(mn);
   drawHourHand(hr);
   setTimeValues();
 }

}

function checkAnswer(){
    var right = $("#minOpt option:selected").text();
    var ans = $("#minVal").val();
    var ok = Number($("#ok").val());
    var xx = Number($("#xx").val());
    var tally = ok + xx;
    if(right == ans){
        $("#ok").val(Number($("#ok").val())+1);
        tally = tally + 1;
    }else{
       $("#xx").val(Number($("#xx").val())+1);
       tally = tally + 1;
    }
     function createClock(){
        var mn = new Object();
        var hr = new Object();
        drawMinuteHand(mn);
        drawHourHand(hr);
        setTimeValues();
    }
    createClock();
}

function refreshTime(){
    
   var canvas = document.getElementById('myCanvas'); 
   var context = canvas.getContext('2d');
   var w = parseInt(canvas.getAttribute('width'));
   var h = parseInt(canvas.getAttribute('height'));

   var x = w/2;
   var y = h/2;
   
   context.clearRect(-x,-y,w,h);
   
    //context.translate(w/2, h/2);
    createClock();
   
   
function degreesToRadians(degrees) {
    return (Math.PI / 180) * degrees;
}

 function drawHourHand(hr){
     
     hr.hours = Math.floor((Math.random()*12)+1);
     newHr = hr.hours;
     hr.getHrs = function() {return hr.hours;};
     var hrs = (hr.getHrs()*30);
     context.save();
     context.fillStyle = 'red';
     context.rotate(degreesToRadians(hrs));
     drawHand(140,7);
     context.restore();
 }

 function drawMinuteHand(mn){ 
    var currentMin =  $("#minOpt option:selected").text();
    //alert(currentMin);
     mn.minutes = Math.floor((Math.random()*12)+1);
     newMins = mn.minutes*5;
     if(newMins == currentMin){
        while(newMins == currentMin){
            mn.minutes = Math.floor((Math.random()*12)+1);
            newMins = mn.minutes*5;
        }
    }
    //alert(newMins);
    mn.getMins = function()   {return mn.minutes;};
    var mins = (mn.getMins()*30);
    context.save();
    context.fillStyle = "RGB(165,0,43)";
    context.rotate( degreesToRadians(mins));
    drawHand(170,8);
    context.restore();
 }
 
 
function setTimeValues(){   //put the random min/hr values into text boxes for use when analysing user input
    document.getElementById('minVal').value = newMins;
    document.getElementById('hrVal').value = newHr;
}

function drawHand(size,thickness){
   thickness = thickness || 4;
   context.beginPath();
   context.moveTo(0,0); // start at center  the following is a closed path
   context.lineTo(thickness*-1, -10);//value left, 10 up
   context.lineTo(0, size * -1);     //up to 0,-1
   context.lineTo(thickness,-10);   //down value right,10 up
   context.lineTo(0,0);             //back to center
   context.fill();
 }

 function createClock(){
   var mn = new Object();
   var hr = new Object();
   drawMinuteHand(mn);
   drawHourHand(hr);
   setTimeValues();
 }
}
  
/*Function for rollover images, changes images
back to original image using the parameters at
calling source which are the tag id and image
name*/
function changeImageBack(imgId,imgNam) {

element=document.getElementById(imgId);

if (element.src.match(imgNam+"Details.png")){

	element.src = "images/"+imgNam+".png";
  }
else
  {
  element.src ="images/"+imgNam+".png";
  }
}

/*Takes in the img Tag id and the name*/
function changeImage(imgId,imgNam) {

element=document.getElementById(imgId);
//
if (element.src.match(imgNam+".png")){

	element.src = "images/"+imgNam+"Details.png";
  }
else
  {
  element.src ="images/"+imgNam+".png";
  }
}
//Used to indicate what page is currently loaded
function showWhere(id) {
	document.getElementById(id).style.backgroundColor = "indianRed";

}
