<%-- 
    Document   : TimeLesson
    Created on : 06-Nov-2013, 16:02:59
    Author     : Mary Martin
--%>
<%@ page import="java.util.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.sql.*" import= "School.*" import= "SchoolDao.*" import="Exceptions.*" %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="myStyles/clockLesson.css">
        <link rel="stylesheet" type="text/css" href="myStyles/commonStyle.css">
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.6.js"></script>
        <script type="text/javascript" src="jsFiles/timeLesson.js"></script>  
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tell The Time</title>
    </head>
    <body onload="setupClock()">
<%
  User current = (User)session.getAttribute("theUser");
  String userName = current.getFname()+" "+current.getLname();
%>

<h3>Lesson Area</h3>

        <div id="score">
            <form action ="SchoolServlet" method = "post" >
                <label for="minOpt">Select A Value For 
               The Minute Hand</label>
                <select id="minOpt" size="1" onchange="checkAnswer()">
                    <option>5</option>
                    <option>10</option>
                    <option>15</option>
                    <option>20</option>
                    <option>25</option>
                    <option>30</option>
                    <option>35</option>
                    <option>40</option>
                    <option>45</option>
                    <option>50</option>
                    <option>55</option>
                    <option>60</option>
                </select>
                <br/><br/><br/>
                <label for="usr">Student Name&nbsp;</label><input type="text" name="usr" id="usr" value="<%=userName%>" readonly="true"/>
                <br/><br/>
                <label for="minVal">Minute value&nbsp;</label><input type="text" name="minVal" id="minVal" />
                <br/><br/>
                <label for="hrVal">Hour value&nbsp;</label><input type="text" name="hrVal" id="hrVal" />
                <br/><br/>
                <label for="lessonOpt">Lesson Type</label>
                <select name="lessonOpt" size="1">
                <%
                LessonDao lessonService = new LessonDao();
                try{
                    List<Lesson> lessons = lessonService.findAllLessons();
                          if(lessons != null){
                                for(Lesson les : lessons){
                                    String name = les.getLessonName();
                %> 
                    <option><%=name%></option> 
                <%
                                }
                         }
                }
                catch(DaoException e){
                    e.printStackTrace();
                }
                %>
                </select>
                <br/><br/>
                <input  type="submit" name="less" id="less" value="SUBMIT" onclick="" />
                <br/><br/>
                <input type="hidden" name="action" value="lesson" />
                  <label for="ok">Correct total&nbsp;</label><input type="text" name="ok" id="ok" />
                <br/><br/>
                <label for="xx">Wrong total&nbsp;</label><input type="text" name="xx" id="xx" />
                <br/><br/> 
            </form>  
        </div>
        <div id="container">
            <canvas id="myCanvas" width="500" height="500"> 
            </canvas>
        </div>
                <div id="lessNav">
          <a href="SchoolServlet?action=logout">
              <img alt="LOGIN" id="logOut" onmouseover="changeImage('logOut','lo')" onmouseout="changeImageBack('logOut','lo')" src='images/lo.png' />
          </a>
          <br><br>
          <img alt="NewTime" id="newTime" onmouseover="changeImage('newTime','newTime')" onmouseout="changeImageBack('newTime','newTime')" src='images/newTime.png' onclick="refreshTime()" />
                </div>
<%
        String lessonMessage = (String)session.getAttribute("lessonMessage");
             if(lessonMessage!=null) 
        {     
%>            
        <div class="messageStyle"><h3><%=lessonMessage%></h3></div>
<% 
            
        } 
        session.removeAttribute("lessonMessage");     
%> 
 
    </body>
</html>