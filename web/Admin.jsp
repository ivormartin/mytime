<%-- 
    Document   : Admin
    Created on : 12-Dec-2013, 19:38:12
    Author     : user
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.sql.*" import="Exceptions.*" import= "School.*" import= "SchoolDao.*" import="java.util.*" import="java.lang.*" %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="myStyles/commonStyle.css" >
        <link rel="stylesheet" type="text/css" href="myStyles/admin.css" >
        <script type="text/javascript" src="jsFiles/commonScripts.js"></script>
        <script type="text/javascript" src="jsFiles/admin.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.6.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body > 
        
        
        <div id="listArea">   
       <%
        /*theStudent is the student the admin has edited, added or deleted .  Used to
          give feedback on action carried out via userMessage.  Exists after selection and
          action carried out*/
        User theStudent = (User)session.getAttribute("student");
        System.out.println("at start of jsp admin, student is "+theStudent);
        //if theStudent is null you either haven't choosen one yet or they have been removed so there will be a message
        if(theStudent==null){
            theStudent = new User(0,"Username","Password","Firstname","Lastname","www.com",123,false);
            String userMessage = (String)session.getAttribute("message");
            if(userMessage!=null){   
       %>
                <h3 class="messageStyle"><%=userMessage%></h3>
       <%
                session.removeAttribute("message");
            }
        }
       %> 
            <p>Select Student From List</p> 
            <a href="SchoolServlet?action=mongo">Get Mongo Files</a> 
       <form id="listForm" action ="SchoolServlet" method = "post" >
        <select id="thelist" multiple="multiple" size="5" onchange="display()">
            
       <%
            UserDao ud = new UserDao();    
            List<User> users = ud.listAllStudents();
            String details = "";
            if (users != null){
              for (User u : users){
                  details = u.getUserId()+" "+u.getFname()+" "+u.getLname();
       %>   
       <option><%=details%></option>
            <%
              }
            }
            %>
        </select>
        <input type="hidden" name="theId" id="theId" />
        <input type="hidden" name="action" value="view" />
        </form>
  <%
        //else{ 
            //System.out.println("the student here in viewcommand is "+theStudent);
  %> 
   </div>
   <div id="adminArea">
  <div class="tablebg">  
    <form action="SchoolServlet" method="post" name ="admin" id="admin" > 
  <table>
    <tr>
      <td>Student Id</td>
      <td>
        <input type="text"  readonly="true" name="id" id="id" value="<%=theStudent.getUserId()%>" />
      </td>
    </tr>  
    <tr>
      <td>Username</td>
      <td>
        <input type="text" name="un" id="un" value="<%=theStudent.getUname()%>" />
      </td>
    </tr>    
    <tr>
      <td>Password</td>
      <td>
        <input type="text" name="pw" id="pw" value="<%=theStudent.getPword()%>" />
      </td>
    </tr>
    <tr>
      <td>First Name</td>
      <td>
        <input type="text" name="fname" id="fname" value="<%=theStudent.getFname()%>" />
      </td>
    </tr>
    <tr>
      <td>Last Name</td>
      <td>
        <input type="text" name="lname" id="lname" value="<%=theStudent.getLname()%>" />
      </td>
    </tr>
    <tr>
      <td>Email</td>
      <td>
        <input type="text" name="email" id="email" value="<%=theStudent.getEmail()%>" />
      </td>
    </tr>
    <tr>
      <td>Class</td>
      <td>
        <input type="text" name="classid" id="classid" value="<%=theStudent.getClassID()%>" />
      </td>
    </tr>
    <tr>
    <input id="informServer" type="hidden" name="action" value="view" />
    </tr>
  </table>    
   </form> 
</div>

 </div>
    <table id="editTable">
            <tr>
                <td class="imgButton" id="editButton" >

                    <img alt="EDIT" id="editStudent" onclick="sendEditToServer()" onmouseover="changeImage('editStudent','editStudent')" onmouseout="changeImageBack('editStudent','editStudent')" src='images/editStudent.png' />
                </td>
           </tr>
           <tr>
                <td class="imgButton" id="deleteButton" >

                  <img alt="DELETE" id="deleteStudent" onclick="sendDeleteToServer()" onmouseover="changeImage('deleteStudent','deleteStudent')" onmouseout="changeImageBack('deleteStudent','deleteStudent')" src='images/deleteStudent.png' />
                </td>
           </tr> 
           <tr>
                <td class="imgButton" id="createButton" >

                    <img alt="CREATE" id="addStudent" onclick="sendCreateToServer()" onmouseover="changeImage('addStudent','addStudent')" onmouseout="changeImageBack('addStudent','addStudent')" src='images/addStudent.png' />
                </td>

           </tr>
           <tr>
                <td class="imgButton" id="viewButton" >

                    <img alt="VIEW" id="viewStudent" onclick="sendViewToServer()" onmouseover="changeImage('viewStudent','viewStudent')" onmouseout="changeImageBack('viewStudent','viewStudent')" src='images/viewStudent.png' />
                </td>
           </tr>
           <tr>
                <td class="imgButton" id="resetButton" >
                    <img alt="RESET" id="resetStudent" onclick="resetFields()" onmouseover="changeImage('resetStudent','resetFields')" onmouseout="changeImageBack('resetStudent','resetFields')" src='images/resetFields.png' />

                </td>
          </tr>
          <tr>
                <td class="imgButton" id="logOutButton" ><a href="SchoolServlet?action=logout">
                    <img alt="LOGIN" id="logOut" onmouseover="changeImage('logOut','logOut')" onmouseout="changeImageBack('logOut','logOut')" src='images/logOut.png' />
                </a></td>
          </tr>
        </table> 
       
      </body>
</html>     