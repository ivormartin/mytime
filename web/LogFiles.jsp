<%-- 
    Document   : LogFiles
    Created on : 29-Apr-2014, 23:35:28
    Author     : user
--%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="myStyles/commonStyle.css" >
        <title>Logs</title>
    </head>
    <body>
        <div id="logDisplay">
        <h1>Logs</h1>
        <h4><a href="SchoolServlet?action=mongo">Get Mongo Files</a></h4><p></p>
        <h4><a href="SchoolServlet?action=deletemongodoc">Delete Mongo File</a></h4>
        <h4><a href="Admin.jsp">Back to Admin Page</a></h4>
        <%
            List<String> logs = (List)session.getAttribute("logs");
            if(logs!=null){
                for(String s: logs){
       %>
                <h3><%=s%></h3>
       <%
                }
            }
                session.removeAttribute("logs");
             
        %>
        </div>
    </body>
</html>
