<%-- 
    Document   : logIn
    Created on : 05-Nov-2013, 20:44:21
    Author     : Mary Martin
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.sql.*" import= "School.*" import= "SchoolDao.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" 
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session" />

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.text" />

<!DOCTYPE html>
<html lang="${language}">
    <head>
        <link rel="stylesheet" type="text/css" href="myStyles/commonStyle.css">
        <link rel="stylesheet" type="text/css" href="myStyles/login.css">
        <script type="text/javascript" src="jsFiles/login.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Log In</title>
    </head>
    <body >
     <form action="logIn.jsp">
            <select id="language" name="language" onchange="submit()">
                <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
                <option value="nl" ${language == 'nl' ? 'selected' : ''}>Nederlands</option>
                <option value="ga" ${language == 'ga' ? 'selected' : ''}>Gaeilge</option>
                <option value="es" ${language == 'es' ? 'selected' : ''}>Español</option>
            </select>
     </form>   
    <div id="login">
 <%
     //newUser set on successfully registering
     User n = (User)session.getAttribute("newUser");
     String welcome = "";
     if (!(n==null))
        { 
            welcome = "Thanks for registering "+n.getFname()+"\n please Login";
            //don't need 'newUser' after this
            session.removeAttribute("newUser");
 %>
   <h2 class="userWelcome" id="userWelcome"><%=welcome%></h2>
<%
        }
     String id = (String)session.getAttribute("loggedSessionId");
     if(id==null){
%>
    <div class="tablebg" >   
        <form action ="SchoolServlet" method="post" id="log"> 
          <label for="un"><fmt:message key="login.label.username" />:</label>
          <input type="text" id="un" name="un"><br>
          <br>
          <label for="pw"><fmt:message key="login.label.password" />:</label>
          <input type="text" id="pw" name="pw"><br>
          <br>
          <input type="hidden" name="action" value="logIn" />
          <fmt:message key="login.button.submit" var="buttonValue" />
          <input class="logInButton" type="submit" value="${buttonValue}">
        </form>
    </div>
<%
     }else{
        User current = (User)session.getAttribute("theUser");
        boolean isTutor = current.getType();
        if(!(isTutor)){
%>
<div class="tablebg">
<h1 class="logInArea">Welcome <%=current.getFname()%> Proceed to <br/><a href="TimeLesson.jsp">Lesson Area</a></h1>
   
<%
        }else{
    %>
    <div class="tablebg">
<h1 class="logInArea">Welcome <%=current.getFname()%> Proceed to <br/><a href="Admin.jsp">Admin Area</a></h1>
     
<%
        }
    %>
<h1 id="logOutNav">Change your mind? <br/><a href="SchoolServlet?action=logout">LOG OUT</a></h1>
    </div>
    <%   
     }
%>
    </div>
    </div>
    </body>
</html>