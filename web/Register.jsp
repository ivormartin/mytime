<%-- 
    Document   : index
    Created on : 05-Nov-2013, 20:42:19
    Author     : Mary Martin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.sql.*" import= "School.User" import= "SchoolDao.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<c:set var="language" 
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
        scope="session"/>

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="i18n.text" />
<html lang="${language}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="myStyles/commonStyle.css">
        <link rel="stylesheet" type="text/css" href="myStyles/register.css">
        <script type="text/javascript" src="jsFiles/commonScripts.js"></script>
        <script type="text/javascript" src="jsFiles/register.js"></script>
        
        <title>Register</title>
    </head>
    <body>
<%
String regMessage = (String)session.getAttribute("failedLoginMessage");
  if(!(regMessage==null))  
  {
%>
        <div class="messageStyle"><h2><%=regMessage%></h2></div>
<%
    session.removeAttribute("failedLoginMessage");
    //onsubmit="return validateForm(this);"
  }
%>
<div id="tablewrapper"> 
<div  class="tablebg">     
 <form action="SchoolServlet" method="post" name ="reg" id="reg"  >     
   <table>
    <tr>
      <td id="enterUn"><fmt:message key="regTable.td.enterUn"/>:</td>
      <td>
        <input type="text"  name="un" id="un" />
      </td>
    </tr>    
    <tr>
      <td id="enterPw"><fmt:message key="regTable.td.enterPw"/>:</td>
      <td>
        <input type="text"  name="pw" id="pw" />
      </td>
    </tr>
    <tr>
      <td id="enterFn"><fmt:message key="regTable.td.enterFn"/>:</td>
      <td>
        <input type="text"  name="fname" id="fname" />
      </td>
    </tr>
    <tr>
      <td id="enterLn"><fmt:message key="regTable.td.enterLn"/>:</td>
      <td>
        <input type="text"  name="lname" id="lname" />
      </td>
    </tr>
     <tr>
      <td id="enterEm"><fmt:message key="regTable.td.enterEm"/>:</td>
      <td>
        <input type="text"  name="email" id="email" />
      </td>
    </tr>
    <tr>
      <td id="enterCl"><fmt:message key="regTable.td.enterCl"/>:</td>
      <td>
        <input type="text"  name="classid" id="classid" />
      </td>
    </tr>
    <tr>
      <td id="enterSorT"><fmt:message key="regTable.td.enterSorT"/>:</td>
      <td>
        <input type="text"  name="bool" id="bool" />
      </td>
    </tr>
    <tr>
      <td>
        <input id="regStudent" type="hidden" name="action" value="register" />
      </td>
    </tr>
  </table>
</form>
 </div> 
</div>
<div id="nav">
    <a href="SchoolServlet?action=logout"><img alt="LOGIN" id="lgIn" onmouseover="changeImage('lgIn','login')" onmouseout="changeImageBack('lgIn','login')" src='images/login.png' />
    </a>
    <img alt="Register" id="register" onclick="registerStudent()" onmouseover="changeImage('register','reg')" onmouseout="changeImageBack('register','reg')" src='images/reg.png' />
    <img alt="Reset" id="reset" onclick="resetFields()" onmouseover="changeImage('reset','resetFields')" onmouseout="changeImageBack('reset','resetFields')" src='images/resetFields.png' />
</div>
        </body>
</html>