/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SchoolDao;

import School.*;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author user
 */
public class UserDaoTest {
    
    public UserDaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of logIn method, of class UserDao.
     */
    @Test
    public void testLogIn() {
        System.out.println("logIn");
        String uname = "";
        String pword = "";
        UserDao instance = new UserDao(new MyDataSource());
        User expResult = null;
        User result = instance.logIn(uname, pword);
        assertEquals("Fail log in Test empty String ",expResult, result);
    }

        @Test
    public void testLogIn2() {
        System.out.println("logIn");
        String uname = null;
        String pword = null;
        UserDao instance = new UserDao(new MyDataSource());
        User expResult = null;
        User result = instance.logIn(uname, pword);
        assertEquals("Fail log in Test null values ",expResult, result);
    }
    
        @Test
    public void testLogIn3() {
        System.out.println("logIn");
        String uname = "lunchbox";
        String pword = "biscuits";
        UserDao instance = new UserDao(new MyDataSource());
        User expResult = null;
        User result = instance.logIn(uname, pword);
        assertEquals("Fail log in Test known incorrect values ",expResult, result);
    }
    
    /**
     * Test of register method, of class UserDao.
     */
    @Test
    public void testRegister() throws Exception {
        System.out.println("register");
        User student = new User(200,"hound","dog","Robert","Mitchum","rob@eircom.net",2,true);
        UserDao instance = new UserDao(new MyDataSource());
        boolean expResult = true;
        boolean result = instance.register(student);
        instance.removeStudent(student);
        assertEquals("Test for registering new User ",expResult, result);
        
    }

    /**
     * Test of listAllStudents method, of class UserDao.
     */
    @Test
    public void testListAllStudents() throws Exception {
        System.out.println("Test listAllStudents");
        UserDao instance = new UserDao(new MyDataSource());
        List<User> userList = instance.listAllStudents();
        boolean result = (userList!=null);
        boolean expResult = true;
        assertEquals("Test listAllStudents ",expResult, result);
        // TODO review the generated test code and remove the default call to fail. 
    }

    /**
     * Test of findUserById method, of class UserDao.
     */
    @Test
    public void testFindUserById() throws Exception {
        System.out.println("findUserById");
        int expResult = 200;
        int id = expResult;
        User student = new User(id,"hound","dog","Robert","Mitchum","rob@eircom.net",2,true);
        UserDao instance = new UserDao(new MyDataSource());
        instance.register(student);
        User theNewStu = instance.findUserById(id);
        int result = theNewStu.getUserId();
        instance.removeStudent(student);
        assertEquals("Test for findUserById ",expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of removeStudent method, of class UserDao.
     */
    @Test
    public void testRemoveStudent() throws Exception {
        System.out.println("removeStudent");
        User student = new User(200,"hound","dog","Robert","Mitchum","rob@eircom.net",2,true);
        UserDao instance = new UserDao(new MyDataSource());
        instance.register(student);
        int expResult = 1;
        int result = instance.removeStudent(student);
        assertEquals("Test for remove Student",expResult, result);
    }

    /**
     * Test of editStudent method, of class UserDao.
     */
    @Test
    public void testEditStudent() throws Exception {
        System.out.println("editStudent");
        User student = new User(200,"hound","dog","Robert","Mitchum","rob@eircom.net",2,true);
        UserDao instance = new UserDao(new MyDataSource());
        instance.register(student);
        User oldStudent = instance.findUserById(200);
        String newEmail = "houndog@dkit.ie";
        User newStudent = instance.findUserById(200);
        newStudent.setEmail(newEmail);
        instance.editStudent(newStudent);
        boolean expResult = !oldStudent.getEmail().equals(newStudent.getEmail());
        boolean result = newStudent.getEmail().equals(newEmail);
        instance.removeStudent(student);
        assertEquals("Test for testEditStudent ",expResult, result);  
    }  
}